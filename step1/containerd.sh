cd containerd/
wget https://k8s-pod-migration.obs.eu-de.otc.t-systems.com/v2/containerd
git clone https://github.com/SSU-DCN/podmigration-operator.git
cd podmigration-operator
tar -vxf binaries.tar.bz2
cd custom-binaries/
chmod +x containerd
sudo mv containerd /bin/
