This script to install pod migrating for easy way. Ref: https://github.com/SSU-DCN/podmigration-operator/blob/main/init-cluster-containerd-CRIU.md

Clone this code folowing:


```
git clone https://gitlab.com/mindhappyokk/pod-migrating.git
cd pod-migrating/step1
./step1.sh
```

- Step2: Solve a few problems introduced with containerd

```
sudo nano /etc/sysctl.conf
...
net.bridge.bridge-nf-call-iptables = 1

sudo -s
sudo echo '1' > /proc/sys/net/ipv4/ip_forward
exit
sudo sysctl --system
sudo modprobe overlay
sudo modprobe br_netfilter
./step4.sh
./step5.sh
```

`git clone https://github.com/vutuong/kubernetes.git` > on /home/ubuntu/

For master node,

You need to do step 7 

```
sudo kubeadm init --pod-network-cidr=10.244.0.0/16
sudo nano /var/lib/kubelet/config.yaml
  authorization:
    #mode: Webhook
    mode: AlwaysAllow
sudo nano /etc/exports
/var/lib/kubelet/migration/  192.168.10.0/24(rw,sync,no_subtree_check,no_root_squash,no_all_squash)
Note: 192.168.10.0/24 is subnetmask of every node in our cluster

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

kubectl get pods --all-namespaces
kubectl get pods -n kube-system

journalctl -fu kubelet
```


and 

- ./formaster.sh


- Go back to directory "pod-migration" 


```
./step8.sh
sudo cp runc.conf /etc/criu/
```

For worker node,

and step 9 

```
sudo nano /etc/fstab
192.168.10.13:/var/lib/kubelet/migration   /var/lib/kubelet/migration  nfs  defaults,_netdev 0 0
Note: 192.168.10.13 is the IP address of Nfs-server (master node) in this case.
```


and

- ./forworker.sh
- ./installgo.sh


For run pod migrating,

Ref: https://github.com/SSU-DCN/podmigration-operator/tree/main/kubectl-plugin


- To build checkpoint command:

```
cd podmigration-operator/kubectl-plugin/checkpoint-command
go build -o kubectl-checkpoint
sudo cp kubectl-checkpoint /usr/local/bin
```

- To build migrate command:

```
cd migrate-command
go build -o kubectl-migrate
sudo cp kubectl-migrate /usr/local/bin
```

Ref: https://github.com/schrej/podmigration-operator/issues/1 

- since as you can see in my guide (https://github.com/SSU-DCN/podmigration-operator), you should follow the How to run part ( except run the GUI) to make something like the demo video:

Firstly, download the source code in the repo:

`git clone https://github.com/SSU-DCN/podmigration-operator.git`
In one terminal, run the podmigration controller:

```
cd podmigration-operator/
make manifests
sudo snap install kustomize
sudo apt-get install gcc
make install
make run
```


- In other terminal run the API server to listen the request from kubectl migrate command:

```
cd podmigration-operator/
go run ./api-server/cmd/main.go
```


- Finally, because there is no kubectl migrate in the default kubernetes, thus; I wrote the kubectl plugin for it. To install kubectl migrate/checkpoint command, follow the guide at https://github.com/SSU-DCN/podmigration-operator/tree/main/kubectl-plugin
P/S: I should check your cluster to make sure it's boot well before all. Every node should be in Ready status.



