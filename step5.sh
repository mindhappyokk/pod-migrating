git clone https://github.com/SSU-DCN/podmigration-operator.git
cd podmigration-operator
tar -vxf binaries.tar.bz2
cd custom-binaries

chmod +x kubeadm kubelet
sudo mv kubeadm kubelet /usr/bin/
sudo systemctl daemon-reload
sudo systemctl restart kubelet
sudo systemctl status kubelet

