git clone https://github.com/SSU-DCN/podmigration-operator.git
cd podmigration-operator
tar -vxf criu-3.14.tar.bz2
cd criu-3.14
sudo apt-get install protobuf-c-compiler protobuf-compiler \
libprotobuf-dev:amd64 gcc build-essential bsdmainutils python git-core \
asciidoc make htop git curl supervisor libapparmor-dev \
libseccomp-dev libprotobuf-dev protobuf-c-compiler \
protobuf-compiler python-protobuf libnl-3-dev libcap-dev libaio-dev \
apparmor libprotobuf-c-dev libnet1-dev libnl-genl-3-dev libnl-route-3-dev libnfnetlink-dev pkg-config
make clean
make
sudo make install
sudo criu check
sudo criu check --all
sudo mkdir /etc/criu
#touch /etc/criu/runc.conf
sudo cp runc.conf /etc/criu/

